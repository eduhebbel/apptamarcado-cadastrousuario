package view.tabelas;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Usuario;

public class UsuarioTableModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	private static final int COL_NOME = 0;
	private static final int COL_SOBRENOME = 1;
	private static final int COL_EMAIL = 2;
	private static final int COL_SEXO = 3;
	private static final int COL_REGIAO = 4;
	
	private List<Usuario> usuarios;       

	//Esse é um construtor, que recebe a nossa lista de clientes
	public UsuarioTableModel(List<Usuario> usuarios) {
		this.usuarios = new ArrayList<Usuario>(usuarios);
	}

	public int getRowCount() {
		//Quantas linhas tem sua tabela? Uma para cada item da lista.
		return usuarios.size();
	}

	public int getColumnCount() {
		//Quantas colunas tem a tabela? Nesse exemplo, sï¿½ 2.
		return 5;
	}

	public String getColumnName(int column) {
		//Qual é o nome das nossas colunas?
		if (column == COL_NOME) return "Nome:";
		if (column == COL_SOBRENOME) return "Sobrenome:";
		if (column == COL_EMAIL) return "E-mail:";
		if (column == COL_SEXO) return "Sexo:";
		if (column == COL_REGIAO) return "Regiao:";
		return ""; //Nunca deve ocorrer
	}

	public Object getValueAt(int row, int column) {
		//Precisamos retornar o valor da coluna column e da linha row.
		Usuario usuario = usuarios.get(row);
		if (column == COL_NOME)
			return usuario.getNome();
		if (column == COL_SOBRENOME)
			return usuario.getSobrenome();
		if (column == COL_EMAIL)
			return usuario.getEmail();
		if (column == COL_SEXO)
			return usuario.getSexo();
		if (column == COL_REGIAO)
			return usuario.getRegiao();
		return ""; //Nunca deve ocorrer
	}

	

	public Class<?> getColumnClass(int columnIndex) {
		//Qual a classe das nossas colunas? Como estamos exibindo texto, ï¿½ string.
		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		//Indicamos se a célula da rowIndex e da columnIndex é editável. Nossa tabela toda ï¿½.
		return false;
	}
	//Já que esse tableModel é de clientes, vamos fazer um get que retorne um objeto cliente inteiro.
	//Isso elimina a necessidade de chamar o getValueAt() nas telas. 
	public Usuario get(int row) {
		return usuarios.get(row);
	}
}

