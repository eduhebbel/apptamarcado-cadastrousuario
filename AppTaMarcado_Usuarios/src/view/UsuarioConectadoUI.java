package view;

import java.awt.EventQueue;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.UsuarioController;
import model.Regiao;
import model.Sexo;
import model.Usuario;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class UsuarioConectadoUI extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtSobrenome;
	private JTextField txtDataNasc;
	private JTextField txtEmail;
	private JTextField txtLogin;
	private final ButtonGroup Sex = new ButtonGroup();
	private JTextField txtNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsuarioConectadoUI frame = new UsuarioConectadoUI(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UsuarioConectadoUI(Usuario usuario) {
		try {
			setMaximum(true);
		} catch (PropertyVetoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setBounds(100, 100, 388, 337);
		
		JLabel lblSobrenome = new JLabel("Sobrenome: ");
		
		JLabel lblDataNascimento = new JLabel("Data Nascimento: ");
		
		JLabel lblEmail = new JLabel("E-mail: ");
		
		JLabel lblLogin = new JLabel("Login: ");
		
		JLabel lblRegio = new JLabel("Regi\u00E3o: ");
		
		JLabel lblSexo = new JLabel("Sexo: ");
		
		JComboBox cBoxRegiao = new JComboBox();
		cBoxRegiao.setEnabled(false);
		cBoxRegiao.setModel(new DefaultComboBoxModel(Regiao.values()));
		cBoxRegiao.setSelectedIndex(usuario.getCodigoRegiao());
		
		txtSobrenome = new JTextField();
		txtSobrenome.setEditable(false);
		txtSobrenome.setColumns(10);
		txtSobrenome.setText(usuario.getSobrenome());
		
		
		txtDataNasc = new JTextField();
		txtDataNasc.setEditable(false);
		txtDataNasc.setColumns(10);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dtNsc = sdf.format(usuario.getDataNasc());
		txtDataNasc.setText(dtNsc);
		
		txtEmail = new JTextField();
		txtEmail.setEditable(false);
		txtEmail.setColumns(10);
		txtEmail.setText(usuario.getEmail());
		
		JRadioButton rdbtnFeminino = new JRadioButton("Feminino");
		rdbtnFeminino.setEnabled(false);
		Sex.add(rdbtnFeminino);
		
		JRadioButton rdbtnMasculino = new JRadioButton("Masculino");
		rdbtnMasculino.setEnabled(false);
		Sex.add(rdbtnMasculino);
		
		
		if(usuario.getSexo().toString().equals("MASCULINO")) {
			rdbtnMasculino.setSelected(true);
		}
		else {
			rdbtnFeminino.setSelected(true);
		}
		
		txtLogin = new JTextField();
		txtLogin.setEditable(false);
		txtLogin.setColumns(10);
		txtLogin.setText(usuario.getLogin());
		
		JLabel lblNome = new JLabel("Nome: ");
		
		txtNome = new JTextField();
		txtNome.setEditable(false);
		txtNome.setColumns(10);
		txtNome.setText(usuario.getNome());
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtLogin.setEditable(true);
				txtSobrenome.setEditable(true);
				txtNome.setEditable(true);
				txtDataNasc.setEditable(true);
				cBoxRegiao.setEditable(true);
				cBoxRegiao.setEnabled(true);
				rdbtnFeminino.setEnabled(true);
				rdbtnMasculino.setEnabled(true);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String regiao = cBoxRegiao.getSelectedItem().toString();
				try {
					Date dtNsc = sdf.parse(txtDataNasc.getText());
					usuario.setNome(txtNome.getText());
					usuario.setSobrenome(txtSobrenome.getText());
					usuario.setDataNasc(dtNsc);
					if(rdbtnFeminino.isSelected()) {
						usuario.setSexo(Sexo.FEMININO);
					}
					else {
						usuario.setSexo(Sexo.MASCULINO);
					}
					usuario.setRegiao(Regiao.valueOf(regiao));

				}catch(ParseException arg0) {
					// TODO Auto-generated catch block
					arg0.printStackTrace();
				}
				new UsuarioController().editar(usuario);
				rdbtnFeminino.setEnabled(false);
				rdbtnMasculino.setEnabled(false);
				txtLogin.setEditable(false);
				txtSobrenome.setEditable(false);
				txtNome.setEditable(false);
				txtDataNasc.setEditable(false);
				cBoxRegiao.setEditable(false);
				cBoxRegiao.setEnabled(false);
				JOptionPane.showMessageDialog(null, "Edi��o Realizada com sucesso!");
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblRegio)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(cBoxRegiao, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblDataNascimento)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtDataNasc, 172, 172, 172))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblEmail)
								.addComponent(lblSexo))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(rdbtnFeminino)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(rdbtnMasculino))
								.addComponent(txtEmail, 217, 217, 217)))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(lblNome)
								.addGap(18)
								.addComponent(txtNome))
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(lblSobrenome)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(txtSobrenome, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnSalvar)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblLogin)
									.addGap(18)
									.addComponent(txtLogin, 220, 220, 220)))))
					.addContainerGap(75, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNome)
						.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblSobrenome)
						.addComponent(txtSobrenome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtDataNasc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblDataNascimento))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEmail)
						.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(rdbtnFeminino)
							.addComponent(lblSexo))
						.addComponent(rdbtnMasculino))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblRegio)
						.addComponent(cBoxRegiao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLogin)
						.addComponent(txtLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnSalvar)
					.addContainerGap(26, Short.MAX_VALUE))
		);
		getContentPane().setLayout(groupLayout);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnEditarPerfil = new JMenu("Editar Perfil");
		menuBar.add(mnEditarPerfil);
		
		JMenuItem mntmEditar = new JMenuItem("Editar");
		mntmEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtLogin.setEditable(true);
				txtSobrenome.setEditable(true);
				txtNome.setEditable(true);
				txtDataNasc.setEditable(true);
				cBoxRegiao.setEditable(true);
				cBoxRegiao.setEnabled(true);
			}
		});
		mnEditarPerfil.add(mntmEditar);
		
		JMenu mnLogout = new JMenu("Logout");
		menuBar.add(mnLogout);
		
		JMenuItem mntmLogout = new JMenuItem("Logout");
		mnLogout.add(mntmLogout);
		
		JMenu mnDeletarPerfil = new JMenu("Deletar Perfil");
		menuBar.add(mnDeletarPerfil);
		
		JMenuItem mntmExcluirConta = new JMenuItem("Excluir Conta");
		mntmExcluirConta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new UsuarioController().deletarConta(usuario);
				JOptionPane.showMessageDialog(null, "Conta Exlu�da com Sucesso!");
			}
		});
		mnDeletarPerfil.add(mntmExcluirConta);

	}
}
