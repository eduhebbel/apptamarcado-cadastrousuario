package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.UsuarioController;
import model.Usuario;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroSenhaUI extends JInternalFrame {
	private JPasswordField pswd;
	private JPasswordField pswdConfirm;
	private Usuario usuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroSenhaUI frame = new CadastroSenhaUI(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroSenhaUI(Usuario usuario) {
		setClosable(true);
		setIconifiable(true);
		setMaximizable(true);
		setBounds(100, 100, 331, 209);
		this.usuario = usuario;
		
		
		JLabel lblDigiteSuaSenha = new JLabel("Digite sua Senha: ");
		
		
		JLabel lblConfirmeSuaSenha = new JLabel("Confirme sua Senha: ");
		
		pswd = new JPasswordField();
		pswd.setToolTipText("Senha deve ter no m\u00EDnimo 8 caracteres sendo ao menos uma letra, um n\u00FAmero e um caractere especial");
		
		pswdConfirm = new JPasswordField();
		pswdConfirm.setToolTipText("Senha Deve ser igual ao campo anterior");
		
		JButton btnCadastrar = new JButton("Concluir Cadastro");
		btnCadastrar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				char[] ch = pswd.getPassword();
				String pswd1 = "";
				String pswd2 = "";
				for (int i = 0; i < ch.length;i++) {
					pswd1 += ch[i];
				}
				ch = pswdConfirm.getPassword();
				for (int i = 0; i < ch.length;i++) {
					pswd2 += ch[i];
				}
				System.out.println(pswd1);
				System.out.println(pswd2);
				if(pswd1.equals(pswd2)) {
					new UsuarioController().cadastrar(usuario);
					JOptionPane.showMessageDialog(null,"Cadastro Realizado com Sucesso!");
					dispose();
				}
				else {
					JOptionPane.showMessageDialog(null, "Senha Digitada n�o Confere");
				}
			}
		});
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(45)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnCadastrar)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnCancelar))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblConfirmeSuaSenha)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(pswdConfirm))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblDigiteSuaSenha)
							.addGap(18)
							.addComponent(pswd, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(58, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDigiteSuaSenha)
						.addComponent(pswd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblConfirmeSuaSenha)
						.addComponent(pswdConfirm, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCadastrar)
						.addComponent(btnCancelar))
					.addContainerGap(37, Short.MAX_VALUE))
		);
		getContentPane().setLayout(groupLayout);

	}
}
