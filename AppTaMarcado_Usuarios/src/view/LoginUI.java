package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.UsuarioController;
import model.Usuario;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtLogin;
	private JPasswordField pswdLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginUI frame = new LoginUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginUI() {
		setTitle("App T\u00E1 Marcado!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 367, 241);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblLogin = new JLabel("E-mail ou Login: ");
		
		JLabel lblSenha = new JLabel("Senha: ");
		
		txtLogin = new JTextField();
		txtLogin.setColumns(10);
		
		pswdLogin = new JPasswordField();
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				System.out.println(txtLogin.getText());
//				System.out.println(usercon.getNome());
//				System.out.println(usercon.getEmail());
//				System.out.println(usercon.getSobrenome());
//				System.out.println(usercon.getSexo());
//				System.out.println(usercon.getRegiao());
//				System.out.println(usercon.getCodigoRegiao());
//				System.out.println(usercon.getLogin());
//				System.out.println(usercon.getDataNasc());
				try {
					Usuario usercon = new UsuarioController().usuarioConectado(txtLogin.getText());
					UsuarioConectadoUI usuarioConectado = new UsuarioConectadoUI(usercon);
					usuarioConectado.setVisible(true);
					contentPane.add(usuarioConectado, 0);
				}
				catch(Exception arg) {
					arg.printStackTrace();
					JOptionPane.showMessageDialog(null, "Login e/ou Senha Inv�lida!");
				}
				
			}
		});
		
		JButton btnNovoUsurio = new JButton("Novo Usu\u00E1rio");
		btnNovoUsurio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastroNovoUsuarioUI cadUsuario = new CadastroNovoUsuarioUI();
				cadUsuario.setVisible(true);
				contentPane.add(cadUsuario,0);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(42)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnEntrar)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnNovoUsurio))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblLogin)
								.addComponent(lblSenha))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(pswdLogin)
								.addComponent(txtLogin, GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE))))
					.addContainerGap(128, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblLogin)
						.addComponent(txtLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblSenha)
						.addComponent(pswdLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(49)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnEntrar)
						.addComponent(btnNovoUsurio))
					.addContainerGap(91, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
