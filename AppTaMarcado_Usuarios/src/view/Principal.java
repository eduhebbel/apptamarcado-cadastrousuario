 package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 446);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCadastrese = new JMenu("Cadastre-se");
		menuBar.add(mnCadastrese);
		
		JMenuItem mntmNovoUsuario = new JMenuItem("Novo Usu\u00E1rio");
		mntmNovoUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastroNovoUsuarioUI cadUsuario = new CadastroNovoUsuarioUI();
				cadUsuario.setVisible(true);
				contentPane.add(cadUsuario,0);
			}
		});
		mnCadastrese.add(mntmNovoUsuario);
		
		JMenu mnLogin = new JMenu("Login");
		menuBar.add(mnLogin);
		
		JMenuItem mntmEntrar = new JMenuItem("Entrar");
		mntmEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginUI login = new LoginUI();
				login.setVisible(true);
				contentPane.add(login,0);
			}
		});
		mnLogin.add(mntmEntrar);
		
		JMenu mnListar = new JMenu("Listar");
		menuBar.add(mnListar);
		
		JMenuItem mntmUsurios = new JMenuItem("Usu\u00E1rios");
		mnListar.add(mntmUsurios);
		
		JMenuItem mntmGrupos = new JMenuItem("Grupos");
		mnListar.add(mntmGrupos);
		
		JMenuItem mntmEsportes = new JMenuItem("Esportes");
		mnListar.add(mntmEsportes);
		
		JMenuItem mntmEventos = new JMenuItem("Eventos");
		mnListar.add(mntmEventos);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 303, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(224, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(163, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
