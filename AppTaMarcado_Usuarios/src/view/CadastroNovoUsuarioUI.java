package view;

import model.Usuario;

import java.awt.CardLayout;
import java.awt.EventQueue;
import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.UsuarioController;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import model.Regiao;
import model.Sexo;
import model.Usuario;

import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;

public class CadastroNovoUsuarioUI extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtNome;
	private JTextField txtSobrenome;
	private JTextField txtDataNasc;
	private JTextField txtEmail;
	private JTextField txtConfEmail;
	private final ButtonGroup btnGroupSexo = new ButtonGroup();
	private JTextField txtLogin;
	private Usuario usuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroNovoUsuarioUI frame = new CadastroNovoUsuarioUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void mostrarFrame() {
		
	}

	/**
	 * Create the frame.
	 */
	public CadastroNovoUsuarioUI() {
		try {
			setMaximum(true);
		} catch (PropertyVetoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setBounds(100, 100, 371, 399);
		
		JInternalFrame internalFrame = new JInternalFrame("Cadastro de Usu\u00E1rio");
		internalFrame.setMaximizable(true);
		internalFrame.setIconifiable(true);
		internalFrame.setClosable(true);
		internalFrame.setVisible(true);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addComponent(internalFrame, GroupLayout.PREFERRED_SIZE, 297, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(34, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(internalFrame, GroupLayout.PREFERRED_SIZE, 336, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		
		JLabel lblNome = new JLabel("Nome: ");
		
		JLabel lblSobrenome = new JLabel("Sobrenome");
		
		JLabel lblDataNascimento = new JLabel("Data Nascimento:");
		
		JLabel lblLogin = new JLabel("E-mail: ");
		
		JLabel lblRegio = new JLabel("Regi\u00E3o: ");
		
		JLabel lblSexo = new JLabel("Sexo: ");
		
		JLabel lblConfirmarEmail = new JLabel("Confirmar E-mail: ");
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		
		txtSobrenome = new JTextField();
		txtSobrenome.setColumns(10);
		
		txtDataNasc = new JTextField();
		txtDataNasc.setColumns(10);
		
		JComboBox cBoxRegiao = new JComboBox();
		cBoxRegiao.setMaximumRowCount(6);
		cBoxRegiao.setModel(new DefaultComboBoxModel(Regiao.values()));
		cBoxRegiao.setSelectedIndex(0);
		
		JRadioButton rdbtnFeminino = new JRadioButton("Feminino");
		btnGroupSexo.add(rdbtnFeminino);
		rdbtnFeminino.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//TODO Mudar sele��o RadioButton
			}
		});
		
		JRadioButton rdbtnMasculino = new JRadioButton("Masculino");
		btnGroupSexo.add(rdbtnMasculino);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		
		txtConfEmail = new JTextField();
		txtConfEmail.setColumns(10);
		
		JButton btnProximo = new JButton("Pr\u00F3ximo");
		btnProximo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String msg;
				String regiao = cBoxRegiao.getSelectedItem().toString();
				usuario = new Usuario();
				try {
					Date dtNsc = sdf.parse(txtDataNasc.getText());
					if (txtEmail.getText().equals(txtConfEmail.getText())) {
						usuario.setNome(txtNome.getText());
						usuario.setSobrenome(txtSobrenome.getText());
						usuario.setDataNasc(dtNsc);
						usuario.setEmail(txtEmail.getText());
						if(rdbtnFeminino.isSelected()) {
							usuario.setSexo(Sexo.FEMININO);
						}
						else {
							usuario.setSexo(Sexo.MASCULINO);
						}
						usuario.setRegiao(Regiao.valueOf(regiao));
						usuario.setLogin(txtLogin.getText());
						
					
						CadastroSenhaUI cadSenha = new CadastroSenhaUI(usuario);
						cadSenha.setVisible(true);
						internalFrame.moveToBack();
						cadSenha.moveToFront();
						getParent().add(cadSenha,0);
					}
					else {
						msg = "Erro de Cadastro, Tente novamente!";
						JOptionPane.showMessageDialog(null, msg);
					}
					
				}catch(ParseException arg0) {
					// TODO Auto-generated catch block
					arg0.printStackTrace();
				}
			}
		});
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int returnValue = JOptionPane.showConfirmDialog(null, "Pressione OK para Cancelar Cadastro", "Confirmar Cancelamento", 2);
				if(returnValue == JOptionPane.OK_OPTION) {
					dispose();
					
				}
				
				
			}
		});
		
		JLabel lblLogin_1 = new JLabel("Login: ");
		
		txtLogin = new JTextField();
		txtLogin.setColumns(10);
		GroupLayout groupLayout_1 = new GroupLayout(internalFrame.getContentPane());
		groupLayout_1.setHorizontalGroup(
			groupLayout_1.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_1.createSequentialGroup()
					.addGap(10)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblNome)
							.addGap(10)
							.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblSobrenome)
							.addGap(10)
							.addComponent(txtSobrenome, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblDataNascimento)
							.addGap(10)
							.addComponent(txtDataNasc, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblRegio)
							.addGap(10)
							.addComponent(cBoxRegiao, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblSexo)
							.addGap(6)
							.addComponent(rdbtnFeminino)
							.addGap(18)
							.addComponent(rdbtnMasculino))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblLogin)
							.addGap(10)
							.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblConfirmarEmail)
							.addGap(10)
							.addComponent(txtConfEmail, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(lblLogin_1)
							.addGap(10)
							.addComponent(txtLogin, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addComponent(btnProximo)
							.addGap(37)
							.addComponent(btnCancelar))))
		);
		groupLayout_1.setVerticalGroup(
			groupLayout_1.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_1.createSequentialGroup()
					.addGap(13)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblNome))
						.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblSobrenome))
						.addComponent(txtSobrenome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblDataNascimento))
						.addComponent(txtDataNasc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblRegio))
						.addComponent(cBoxRegiao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(2)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(9)
							.addComponent(lblSexo))
						.addComponent(rdbtnFeminino)
						.addComponent(rdbtnMasculino))
					.addGap(6)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblLogin))
						.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblConfirmarEmail))
						.addComponent(txtConfEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(6)
							.addComponent(lblLogin_1))
						.addComponent(txtLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(45)
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addComponent(btnProximo)
						.addComponent(btnCancelar)))
		);
		internalFrame.getContentPane().setLayout(groupLayout_1);
		getContentPane().setLayout(groupLayout);

	}

	public JTextField getTxtNome() {
		return txtNome;
	}

	public void setTxtNome(JTextField txtNome) {
		this.txtNome = txtNome;
	}

	public JTextField getTxtSobrenome() {
		return txtSobrenome;
	}

	public void setTxtSobrenome(JTextField txtSobrenome) {
		this.txtSobrenome = txtSobrenome;
	}

	public JTextField getTxtDataNasc() {
		return txtDataNasc;
	}

	public void setTxtDataNasc(JTextField txtDataNasc) {
		this.txtDataNasc = txtDataNasc;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JTextField getTxtConfEmail() {
		return txtConfEmail;
	}

	public void setTxtConfEmail(JTextField txtConfEmail) {
		this.txtConfEmail = txtConfEmail;
	}

	public JTextField getTxtLogin() {
		return txtLogin;
	}

	public void setTxtLogin(JTextField txtLogin) {
		this.txtLogin = txtLogin;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ButtonGroup getBtnGroupSexo() {
		return btnGroupSexo;
	}
}
