package controller;

import java.util.ArrayList;

import dao.EsporteDAO;
import model.Esporte;
import model.Usuario;

public class EsporteController {
	
	public ArrayList<Esporte> listarEsportes() {
		return new EsporteDAO().listarEsportes();
	}
	
	public void addEsporte(Esporte esporte) {
		 new EsporteDAO().addEsporte(esporte);
	}
	
	public void delEsporte(Esporte esporte) {
		new EsporteDAO().delEsporte(esporte);
	}
	
	public ArrayList<Esporte> listarEsportesUsuario(Usuario usuario){
		return new EsporteDAO().listarEsportesUsuario(usuario);
	}
	
	
}
