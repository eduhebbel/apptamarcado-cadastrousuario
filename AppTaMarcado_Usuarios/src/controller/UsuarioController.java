package controller;

import java.util.List;

import dao.UsuarioDAO;
import model.Usuario;

public class UsuarioController {
	
	//Controller Cadastro Usuario no Banco de dados
	public void cadastrar(Usuario usuario) {
		new UsuarioDAO().cadastrar(usuario);
	}
	
	//Controller Edi��o Usuario no Banco de dados
	public void editar(Usuario usuario) {
		new UsuarioDAO().editar(usuario);
	}
	
	//Controller Listar Todos Usuario no Banco de dados
	public List<Usuario> buscarTodos(){
		return new UsuarioDAO().buscarTodos();
	}
	
	//Controller Deletar Conta Usuario no Banco de dados
	public void deletarConta(Usuario usuario) {
		new UsuarioDAO().deletarUsuario(usuario);
	}
	
	//Controller Buscar Usuario Conectado 
	public Usuario usuarioConectado(String login) {
		return new UsuarioDAO().usuarioConectado(login);
		
	}
}
