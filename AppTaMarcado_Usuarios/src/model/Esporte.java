package model;


public class Esporte {
	
	private int idEsporte;
	private String esporte;
	
	
	public int getIdEsporte() {
		return idEsporte;
	}
	public void setIdEsporte(int idEsporte) {
		this.idEsporte = idEsporte;
	}
	public String getEsporte() {
		return esporte;
	}
	public void setEsporte(String esporte) {
		this.esporte = esporte;
	}
}
