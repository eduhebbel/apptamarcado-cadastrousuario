package model;

public enum Sexo {

	MASCULINO("Masculino",1),
	FEMININO("Feminino",2);

	private final String nome;
	private final int codigo;

	private Sexo(String nome, int codigo) {
		this.nome = nome;
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public int getCodigo() {
		return codigo;
	}

}
