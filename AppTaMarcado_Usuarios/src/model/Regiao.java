package model;

public enum Regiao {

	SELECIONE("Selecione uma Regi�o",0),
	SUL("Sul",1),
	NORTE("Norte",2),
	CENTRO("Centro",3),
	LESTE("Leste",4),
	CONTINENTE("Continente",5);
	
	private final String regiao;
	private final int codigo;
	
	private Regiao(String regiao, int codigo) {
		this.regiao = regiao;
		this.codigo = codigo;
	}

	public String getRegiao() {
		return regiao;
	}

	public int getCodigo() {
		return codigo;
	}
	
	
}
