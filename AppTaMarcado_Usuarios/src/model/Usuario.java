package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Usuario {
	private int id;
	private String nome;
	private String sobrenome;
	private String login;
	private String email;
	private Date dataNasc;
	private Enum<Sexo> sexo;
	private Regiao regiao;
	private List<Esporte> esportes;
	
	
	public Usuario() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	public Enum<Sexo> getSexo() {
		return sexo;
	}
	public void setSexo(Enum<Sexo> sexo) {
		this.sexo = sexo;
	}
	public String getRegiao() {
		return regiao.getRegiao();
	}
	public int getCodigoRegiao() {
		return regiao.getCodigo();
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	
	public List<Esporte> getEsportes() {
		return esportes;
	}
	public void setEsportes(List<Esporte> esportes) {
		this.esportes = esportes;
	}

	@Override
	public String toString() {
		String nomeCompleto = nome +" "+ sobrenome;
		return nomeCompleto; 
	}
}
	
	
	
	