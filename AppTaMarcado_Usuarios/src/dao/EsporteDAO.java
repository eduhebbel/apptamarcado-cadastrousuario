package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Esporte;
import model.Usuario;
import util.ConnectionUtil;

public class EsporteDAO {
	
	//Conex�o com o Banco de dados
		private static Connection con = ConnectionUtil.getConnection();
		private Usuario user = new Usuario();

	public ArrayList<Esporte> listarEsportes() {
		ArrayList<Esporte> listaEsportes = new ArrayList<Esporte>();
		try {
			String sql = "select * from esporte order by idesporte;";	
			Statement stmt = con.createStatement();
			ResultSet resultado = stmt.executeQuery(sql);
			while(resultado.next()) {
				Esporte esporte = new Esporte();
				esporte.setEsporte(resultado.getString("esporte"));
				esporte.setIdEsporte(resultado.getInt("idesporte"));
				listaEsportes.add(esporte);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listaEsportes;
	}
	
	public void addEsporte(Esporte esporte) {
		String sql = "insert into esporte (esporte) values (?);";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, esporte.getEsporte());
			stmt.execute();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void delEsporte(Esporte esporte) {
		String sql = "delete from esporte where nome = ?;";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, esporte.getEsporte());
			stmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public ArrayList<Esporte> listarEsportesUsuario(Usuario usuario){
		this.user = usuario;
		ArrayList<Esporte> listaEsportes = new ArrayList<Esporte>();
		String sql2 = "select esporte,idesporte from esporte "
				+ "join usuario_esporte on usuario_esporte.id_esporte = esporte.idesporte "
				+ "join usuario on usuario.idusuario = usuario_esporte.id_usuario "
				+ "where usuario.idusuario = " + user.getId()+";";
		try {
			Statement stmt2 = con.createStatement();
			ResultSet resultado2 = stmt2.executeQuery(sql2);
			while(resultado2.next()) {
				Esporte esporte = new Esporte();
				esporte.setEsporte(resultado2.getString("esporte"));
				esporte.setIdEsporte(resultado2.getInt("idesporte"));
				listaEsportes.add(esporte);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaEsportes;
		
	}
	
}
