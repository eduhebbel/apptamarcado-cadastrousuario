package dao;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import controller.EsporteController;
import model.Esporte;
import model.Regiao;
import model.Sexo;
import model.Usuario;
import util.ConnectionUtil;

public class UsuarioDAO {
	
	//Conex�o com o Banco de dados
	private static Connection con = ConnectionUtil.getConnection();
	
	//Cadastrar Usu�rio no Banco de dados
	public void cadastrar(Usuario usuario) {
		//String comando SQL
		String sql = "insert into usuario (nome, sobrenome, login, email, data_nasc, sexo, id_regiao)"
				+ " values (?,?,?,?,?,?,?);";
		try {
			//Preparar Comando
			PreparedStatement stmt = con.prepareStatement(sql);
			//Formatar comando com valores
			stmt.setString(1, usuario.getNome());
			stmt.setString(2, usuario.getSobrenome());
			stmt.setString(3, usuario.getLogin());
			stmt.setString(4, usuario.getEmail());
			//Parse Date para formato Date SQL
			stmt.setDate(5, new Date(usuario.getDataNasc().getTime()));
			stmt.setString(6, usuario.getSexo().toString());
			stmt.setInt(7, usuario.getCodigoRegiao());
			stmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void editar(Usuario usuario) {
		String sql = "update usuario set nome = ?, sobrenome = ?, data_nasc = ?, sexo = ?, regiao = ? where idusuario = ?;";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, usuario.getNome());
			stmt.setString(2, usuario.getSobrenome());
			stmt.setDate(3, new Date(usuario.getDataNasc().getTime()));
			stmt.setString(4, usuario.getSexo().toString());
			stmt.setInt(5, usuario.getCodigoRegiao());
			stmt.setInt(6, usuario.getId());
			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public List<Usuario> buscarTodos(){
		ArrayList<Usuario> lista = new ArrayList<Usuario>();
		try {
			String sql = "select * from usuario;";
			Statement stmt = con.createStatement();
			ResultSet resultado = stmt.executeQuery(sql);
			while(resultado.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(resultado.getInt("idusuario"));
				usuario.setNome(resultado.getString("nome"));
				usuario.setSobrenome(resultado.getString("sobrenome"));
				usuario.setLogin(resultado.getString("login"));
				usuario.setDataNasc(resultado.getDate("data_nasc"));
				usuario.setEmail(resultado.getString("email"));
				usuario.setRegiao(Regiao.valueOf(resultado.getString("regiao").toUpperCase()));
				usuario.setSexo(Sexo.valueOf(resultado.getString("sexo").toUpperCase()));
				String sql2 = "select esporte,idesporte from esporte "
						+ "join usuario_esporte on usuario_esporte.id_esporte = esporte.idesporte "
						+ "join usuario on usuario.idusuario = usuario_esporte.id_usuario "
						+ "where usuario.idusuario = " + usuario.getId();
				Statement stmt2 = con.createStatement();
				ResultSet resultado2 = stmt2.executeQuery(sql2);
				ArrayList<Esporte> listaEsportes = new ArrayList<Esporte>();
				while(resultado2.next()) {
					Esporte esporte = new Esporte();
					esporte.setEsporte(resultado2.getString("esporte"));
					esporte.setIdEsporte(resultado2.getInt("idesporte"));
					listaEsportes.add(esporte);
				}
				usuario.setEsportes(listaEsportes);
				lista.add(usuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}
	
	public void deletarUsuario(Usuario usuario) {
		String sql = "delete from usuario where (id = ? and login = ?);";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, usuario.getId());
			stmt.setString(2, usuario.getLogin());
			stmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Usuario usuarioConectado(String login) {
		Usuario usuarioConectado = new Usuario();
		try {
			//String comando SQL Buscar Usuario 
			String sql = "select * from usuario "
					+ "join regiao on regiao.idregiao = usuario.id_regiao "
					+ "where login like " + "'%" + login + "%'" + " or email like " + "'%" + login + "%'" + ";";
			//Preparar comando
			Statement stmt = con.createStatement();
			//Executar Comando e salvar na vari�vel
			ResultSet resultado = stmt.executeQuery(sql);
			
			if(resultado != null && resultado.next()) {
				//Set Usu�rio com valores da Query
				usuarioConectado.setNome(resultado.getString("nome"));
				usuarioConectado.setSobrenome(resultado.getString("sobrenome"));
				usuarioConectado.setLogin(resultado.getString("login"));
				usuarioConectado.setEmail(resultado.getString("email"));
				usuarioConectado.setDataNasc(resultado.getDate("data_nasc"));
				usuarioConectado.setRegiao(Regiao.valueOf(resultado.getString("regiao").toUpperCase()));
				usuarioConectado.setSexo(Sexo.valueOf(resultado.getString("sexo").toUpperCase()));
				usuarioConectado.setId(resultado.getInt("idusuario"));
				usuarioConectado.setEsportes(new EsporteController().listarEsportesUsuario(usuarioConectado));
			}
		} catch (SQLException e) {
			e.getMessage();
			System.out.println("Login e/ou Senha Inv�lida");
		}
		return usuarioConectado;
		
	}
}
